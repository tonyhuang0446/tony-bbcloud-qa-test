//
//  bbcloud_tony_test_qaApp.swift
//  Shared
//
//  Created by YIYE HUANG on 2022-03-24.
//

import SwiftUI

@main
struct bbcloud_tony_test_qaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
